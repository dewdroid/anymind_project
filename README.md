# Project for Twitter API exam (ANYMIND GROUP)

Requirements
- Python (3.6, 3.7)


Installation
```
pip install -r requirements.txt
python manage.py migrate
python manage.py test
python manage.py runserver 0.0.0.0:8000
```
You can change port to every number you want.
