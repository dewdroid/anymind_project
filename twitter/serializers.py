from rest_framework import serializers


class TweetSerializer(serializers.Serializer):
    limit = serializers.IntegerField(min_value=0, required=False)