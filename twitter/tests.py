from django.conf import settings
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase


class TwitterTest(APITestCase):
    def setUp(self):
        self.CONSUMER_API_KEY = settings.CONSUMER_API_KEY
        self.CONSUMER_API_SECRET = settings.CONSUMER_API_SECRET

    def test_get_hashtag(self):
        url = reverse("twitter-hashtag", args=["python"]) + "?limit=10"
        response = self.client.get(url, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
    
    def test_get_user(self):
        url = reverse("twitter-user", args=["realpython"]) + "?limit=10"
        response = self.client.get(url, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
    
    def test_post_hashtag(self):
        url = reverse("twitter-hashtag", args=["python"]) + "?limit=10"
        response = self.client.post(url, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
    
    def test_post_user(self):
        url = reverse("twitter-user", args=["realpython"]) + "?limit=10"
        response = self.client.post(url, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_get_hashtag_invalid_key(self):
        setattr(settings, 'CONSUMER_API_KEY', 'key')
        setattr(settings, 'CONSUMER_API_SECRET', 'secret')

        url = reverse("twitter-hashtag", args=["python"]) + "?limit=10"
        response = self.client.get(url, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        setattr(settings, 'CONSUMER_API_KEY', self.CONSUMER_API_KEY)
        setattr(settings, 'CONSUMER_API_SECRET', self.CONSUMER_API_SECRET)
    
    def test_get_user_invalid_key(self):
        setattr(settings, 'CONSUMER_API_KEY', 'key')
        setattr(settings, 'CONSUMER_API_SECRET', 'secret')

        url = reverse("twitter-user", args=["realpython"]) + "?limit=10"
        response = self.client.get(url, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        setattr(settings, 'CONSUMER_API_KEY', self.CONSUMER_API_KEY)
        setattr(settings, 'CONSUMER_API_SECRET', self.CONSUMER_API_SECRET)
    
    def test_response_data_hashtag(self):
        url = reverse("twitter-hashtag", args=["python"]) + "?limit=10"
        response = self.client.get(url, content_type='application/json')
        response = response.json()[0]
        self.assertIn("account", response)
        self.assertIn("fullname", response["account"])
        self.assertIn("href", response["account"])
        self.assertIn("id", response["account"])
        self.assertIn("date", response)
        self.assertIn("hashtags", response)
        self.assertIn("retweets", response)
        self.assertIn("likes", response)
        self.assertIn("replies", response)
        self.assertIn("text", response)
    
    def test_response_data_user(self):
        url = reverse("twitter-user", args=["realpython"]) + "?limit=10"
        response = self.client.get(url, content_type='application/json')
        response = response.json()[0]
        self.assertIn("account", response)
        self.assertIn("fullname", response["account"])
        self.assertIn("href", response["account"])
        self.assertIn("id", response["account"])
        self.assertIn("date", response)
        self.assertIn("hashtags", response)
        self.assertIn("retweets", response)
        self.assertIn("likes", response)
        self.assertIn("replies", response)
        self.assertIn("text", response)