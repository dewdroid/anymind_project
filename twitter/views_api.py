import base64
import pytz
import requests
from bs4 import BeautifulSoup, SoupStrainer
from dateutil.parser import parse as date_parse
from django.conf import settings
from rest_framework.exceptions import NotAuthenticated, ParseError
from rest_framework.response import Response
from rest_framework.views import APIView

from .serializers import TweetSerializer


TZ = pytz.timezone("Asia/Bangkok")


class TwitterAPIBase(APIView):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.replies_count_data = {}

    def get_auth_headers(self):
        url = "https://api.twitter.com/oauth2/token"
        data = {"grant_type": "client_credentials"}
        headers = {"Authorization": "Basic " + self.get_bearer_credential()}
        try:
            response = requests.post(url=url, data=data, headers=headers)
        except:
            raise ParseError("Can't connect Twitter.")
        
        if response.status_code == 200:
            response = response.json()
            return {"Authorization": response.get("token_type") + " " + response.get("access_token")}
        else:
            raise NotAuthenticated()

    def get_bearer_credential(self):
        """
            Create Oauth credential
        """
        credential = settings.CONSUMER_API_KEY + ":" + settings.CONSUMER_API_SECRET
        credential_encode = base64.b64encode(credential.encode("utf-8"))
        credential_encode_str = str(credential_encode, "utf-8")
        return credential_encode_str
    
    def get_replies_count(self, tweet):
        """
            >> Optimization <<
            Replies count of retweet equal to its origin tweet.
            So check tweet has origin tweet ("retweeted_status" in JSON)
            if not there --> scraping website to get replies count and store to replies_count_data variable
            if there --> get exist replies count data by tweet id
        """
        if "retweeted_status" in tweet:
            screen_name = tweet["retweeted_status"]["user"]["screen_name"]
            tweet_id = tweet["retweeted_status"]["id_str"]
        else:
            screen_name = tweet["user"]["screen_name"]
            tweet_id = tweet["id_str"]

        if tweet_id in self.replies_count_data:
            return self.replies_count_data[tweet_id]

        try:
            only_span_tags = SoupStrainer(id='profile-tweet-action-reply-count-aria-' + tweet_id)
            html = requests.get('https://twitter.com/' + screen_name + '/status/' + tweet_id)
            soup = BeautifulSoup(html.text, 'lxml', parse_only=only_span_tags)
        except:
            return None
        
        try:
            replies_count = int(soup.find().text.replace(" replies", ""))
        except:
            try:
                replies_count = int(soup.find().text.replace(" reply", ""))
            except:
                replies_count = None

        self.replies_count_data[tweet_id] = replies_count
        return replies_count
    
    def get_pretty_date(self, date_str):
        """
            display date in Asia/Bangkok timezone
        """
        try:
            date = date_parse(date_str).astimezone(TZ)
        except:
            return None
        
        return date.strftime("%-I:%M %p - %-d %b %Y")
    
    def get_pretty_response(self, response):
        """
            customize tweet response in array
        """
        for i, tweet in enumerate(response):
            data = {
                "account": {
                    "fullname": tweet["user"]["name"],
                    "href": "/" + tweet["user"]["screen_name"],
                    "id": tweet["user"]["id"]
                },
                "date": self.get_pretty_date(tweet["created_at"]),
                "hashtags": ["#" + obj["text"] for obj in tweet["entities"]["hashtags"]],
                "retweets": tweet["retweet_count"],
                "likes": tweet["favorite_count"],
                "replies": self.get_replies_count(tweet),
                "text": tweet["text"],
            }
            response[i] = data
        
        return response

    
class HashtagTweetAPI(TwitterAPIBase):

    def get(self, request, hashtag, format=None, *args, **kwargs):
        serializer = TweetSerializer(data=request.GET)
        serializer.is_valid(raise_exception=True)
        limit = serializer.data.get('limit', 30)
        
        url = 'https://api.twitter.com/1.1/search/tweets.json'
        try:
            response = requests.get(url=url,
                                    params={'q': '#' + hashtag, 'count': limit},
                                    headers=self.get_auth_headers())
            response = response.json().get("statuses", [])
        except:
            raise ParseError("Can't connect Twitter.")
        
        response = self.get_pretty_response(response)

        return Response(response)


class UserTweetAPI(TwitterAPIBase):

    def get(self, request, user, format=None, *args, **kwargs):
        serializer = TweetSerializer(data=request.GET)
        serializer.is_valid(raise_exception=True)
        limit = serializer.data.get('limit', 30)
        
        url = 'https://api.twitter.com/1.1/statuses/user_timeline.json'
        try:
            response = requests.get(url=url,
                                    params={'screen_name': user, 'count': limit},
                                    headers=self.get_auth_headers())
        except:
            raise ParseError("Can't connect Twitter.")
        
        if response.status_code != 200:
            raise ParseError("Can't connect Twitter.")
        
        response = self.get_pretty_response(response.json())

        return Response(response)